#un ejemplo de clase para empleados
#def init es el constructor
#python tutor es para que vayas viendo como se ejecuta el programa de python
#difernecia entre clase y objeto, objeto es una copia (empleado: pepe, 20000) es un objeto, aqui hay 2 (=instancia)
#clase = 1 en este código

class Empleado:
    def __init__(self, n, s): #las barrabajas sopn cosas de python
        self.nombre = n 
        self.nomina = s
    
    def calculo_impuestos(self): #al meter estas funciones en class, los parámetros pertenecen a la classs, son funciones especiales para los empleados porque está dentro con todos los datos
        impuestos = self.nomina*0.30
        return impuestos
    
    def __str__(self): #hemos cambiado las funciones en métodos
        return("El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calculo_impuestos()))

empleadoPepe = Empleado ('Pepe', 20000)
empleadaAna = Empleado ('Ana', 30000)
print("El nombre del empleado es {}".format(empleadoPepe.nombre))

total = empleadoPepe.calculo_impuestos() + empleadaAna.calculo_impuestos()
print(empleadoPepe)
print(empleadaAna)

#empleadaAna.imprime()
#empleadoPepe.imprime()
print("Los impuestos a pagar en total son {:.2f} euros".format(total))

#lo del format mete lo siguiente(empleadoPepe la característica del nomnbre) en el hueco
#parecido a lo de self.nombre ponemos un a instanciua(objero) y un punto, después uno de los datos de la clase para sacarlo
#metodos(orientacion a objeros): 'instancia, quiero que me hagas esto'
#self comodín para decir yo, para decir quien somos lo ponemos por delante = INSTANCIA