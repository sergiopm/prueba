class Rectangulo:
    def __init__(self, b, h):
        self.base = b
        self.altura = h
    
    def calculo_area(self):
        area = self.base * self.altura
        return area

    def calculo_perimetro(self):
        perímetro = self.base *2 + self.altura *2
        return perímetro

    def __str__(self):
        return ("Este rectangulo de base {base} y altura {altura} tiene de area {a:.2f} y un perímetro de {p:.2f}".format(base= self.base, altura= self.altura, a= self.calculo_area(), p= self.calculo_perimetro()))
    


rectangulo1 = Rectangulo (25, 10)
print(rectangulo1)
